package com.innominds.flipbazar.entities;
import  com.innominds.flipbazar.entities.dependencies.Rating;
import java.util.HashSet;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
/**
 * 
 * This is an implementation for product class.
 * 
 * 
 * 
 * @author tchavala
 *
 */

public class Product 
{
	public static HashSet<Product> listOfProducts=new HashSet<Product>();
	public static int id_counter=1000;
    private String  p_id;
 String   name;
 String   type;
 float   price;
 String    description;
 
 
 ArrayList<Rating>  ratings;
 public Product() 
 {
	
}
public Product(String name,String type,float price,String description)throws Exception
 {
	if(name.trim().length()<2)
	{
		throw new Exception("name is too short");
	}else if(price<1)
	{
		throw new Exception("price has a negative value");
	}else if(description.trim().length()<10)
	{
		throw new Exception("description is too short");
	}else {
		
	 this.p_id=++id_counter+"";
	 this.name=name;
	 this.type=type;
	 this.price=price;
	 this.description=description;
	 this.ratings= new ArrayList<Rating>();
	//System.out.println("p_id"+ "            "+this.p_id+"\n"+"pname"+"           "+this.name+"\n "+"ptype"+"          "+this.type+"\n "+"price"+"           "+this.price+"\n"+"description"+"       "+this.description);
	//System.out.println("....................................");
	
	 listOfProducts.add(this);
 }
 }
public String toString()
{
	String StringToReturn="this is used to provide product object information\n";
	StringToReturn +="p_id:"+"      " +this.p_id+      "\n";
	StringToReturn +="pname:"+"      "+this.name+"\n";
	StringToReturn +="type:"+"       "+this.type+"\n";
	StringToReturn +="price:"+"       "+this.price+"\n";
	StringToReturn +="description"+"    "+this.description+"\n";
	StringToReturn +=".................................................................";
     return StringToReturn;
}
   







	public static HashSet<Product> searchProducts(String searchString) 
	{
		HashSet<Product> result = new HashSet<Product>();
		
		for (Iterator<Product> iter = listOfProducts.iterator(); iter.hasNext();) 
		{
			Product currentProduct = (Product) iter.next();
			
			if (currentProduct.name.contains(searchString) || currentProduct.description.contains(searchString)) {
				result.add(currentProduct);
			}
		}
         return result;
}
}
