package com.innominds.flipbazar.entities.dependencies;

public class Rating 
{
  private String  review;
  private String  cust_id;
  private int     number;
  
  public Rating()
  {
	  System.out.println("Rating list is created");
  }
  
 public Rating(String review,String cust_id,int number)
  {
	 this.review=review;
	 this.cust_id=cust_id;
	 this.number=number;
  }
}
